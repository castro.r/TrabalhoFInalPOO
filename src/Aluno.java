/*
 * Copyright (c) 2018.
 *
 * Created by Rafael de Castro.
 */

import java.io.Serializable;
import java.util.ArrayList;

public class Aluno implements Serializable {
    private String matricula;
    private String nome;
    private boolean participa;
    private int faltas;
    private int atividades;
    private Gamification newGame;
    private ArrayList<Double> notas = new ArrayList<Double>();

    Aluno(String nome, String matricula) {
        this.matricula = matricula;
        this.nome = nome;
        this.faltas = 0;
        this.atividades = 0;
    }

    // setters
    public void setParticipa(boolean participa) {
        this.participa = participa;
    }

    public void setFaltas(int faltas) {
        this.faltas = faltas;
    }

    public void setAtividades(int atividades) {
        this.atividades = atividades;
    }

    // getters
    public String getMatricula() {
        return matricula;
    }

    public String getNome() {
        return nome;
    }

    public int getFaltas() {
        return faltas;
    }

    public boolean isParticipa() {
        return participa;
    }

    public int getAtividades() {
        return atividades;
    }

    // outros métodos
    public double calculaMedia(){
        int n = notas.size();
        double sum = 0;

        for (Double nota : notas)
            sum += nota;

        return (sum / n);
    }

    // cria um usuário para o aluno
    public void fazerCadastro(String nickName, String senha, int cod, String prof, String cpf){
       newGame = new Gamification(prof, cpf);
       newGame.professor.jogadores.add(new AlunoJogador(this.nome, this. matricula, nickName, senha, cod));
    }

    // adiciona as n notas do aluno em uma lista
    public void addNota(double nota){
        notas.add(nota);
    }

    @Override
    public String toString() {
        return "\nNome aluno: " + nome + "\nMatrícula: " + matricula;
    }
}
