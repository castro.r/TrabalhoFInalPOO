/*
 * Copyright (c) 2018.
 *
 * Created by Rafael de Castro.
 */

import javax.swing.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

public class AlunoJogador extends Aluno implements Observer, Serializable {
    private int codigoID;
    private String senha;
    private String nomeUsuario;
    private String classe;
    private int xp;
    private int level;
    private ArrayList<String> conquistas = new ArrayList<String>();

    AlunoJogador(String nome, String matricula, String nick, String senha, int cod) {
        super(nome, matricula);
        this.nomeUsuario = nick;
        this.senha = senha;
        this.codigoID = cod;
        this.xp = 0;
        this.level = 0;
    }

    // getters
    public String getSenha() {
        return senha;
    }

    public int getCodigoID() {
        return codigoID;
    }

    public String getNomeUsuario() {
        return nomeUsuario;
    }

    public int getLevel() {
        return level;
    }

    public int getXp() {
        return xp;
    }

    public ArrayList<String> getConquistas() {
        return conquistas;
    }

    // setters
    public void setLevel(int level) {
        this.level = level;
    }

    public void setXp(int xp) {
        this.xp = xp;
    }

    public void setClasse(String classe) {
        this.classe = classe;
    }

    // outros metodos
    // atribui uma determinada conquista ao jogador
    public void addConquista(String conq){
        conquistas.add(conq);
    }

    // altera o nome de usuario
    public void modificarUsuario(String newName){
        this.nomeUsuario = newName;
    }

    // altera a senha da conta
    public void modificarSenha(String newPassword){
        this.senha = newPassword;
    }

    // retorna alguns dados do jogador
    @Override
    public String toString() {
        return codigoID + "\t\t" + nomeUsuario + "\t\t\t\t" + classe +
                "\t\t" + level + "\t\t" + conquistas;
    }

    // avisa o jogador toda vez que subir de nível
    @Override
    public void update(Observable o, Object arg) {
        JOptionPane.showMessageDialog(null,this.getNomeUsuario() + arg + "\nLevel " + this.getLevel() + "!");
    }
}
