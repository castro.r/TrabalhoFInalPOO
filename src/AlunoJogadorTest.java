/*
 * Copyright (c) 2018.
 *
 * Created by Rafael de Castro.
 */

import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.*;

public class AlunoJogadorTest {

    @Test
    public void modificarUsuario() {
        AlunoJogador aj = new AlunoJogador("Rafael", "201712295", "Leafar", "senha", 1);
        aj.modificarUsuario("Cybertron");

        assertEquals("Cybertron", aj.getNomeUsuario());
        System.out.println("Teste 1 - Ok!");
    }

    @Test
    public void modificarSenha() {
        AlunoJogador aj = new AlunoJogador("Murilo", "201011222", "Muuh", "password", 2);
        aj.modificarSenha("senha");

        assertEquals("senha", aj.getSenha());
        System.out.println("Teste 2 - Ok!");
    }
}