/*
 * Copyright (c) 2018.
 *
 * Created by Rafael de Castro.
 */

import static org.junit.Assert.*;

public class AlunoTest {

    @org.junit.Test
    public void calculaMedia() {
        Aluno al = new Aluno("Rafael", "201712295");
        al.addNota(7.25);
        al.addNota(8.75);
        al.addNota(7.5);
        al.addNota(8.4);

        assertEquals(7.97, al.calculaMedia(), 0.5);
        System.out.println("Teste 1 - Ok!");
    }
}