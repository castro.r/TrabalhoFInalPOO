/*
 * Copyright (c) 2018.
 *
 * Created by Rafael de Castro.
 */

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.util.ArrayList;

public class FrameInterface extends Component {
    private JButton save;
    private JButton cancel;
    private JButton cadastro;
    private JButton gravar;
    private JButton cod;
    private JButton conq;
    private JButton remove;
    private JFrame myFrame;
    private JTextField matricula;
    private JTextField faltas;
    private JTextField nomeAluno;
    private JTextField notas;
    private JTextField atividades;
    private JRadioButton yes;
    private JRadioButton no;
    private ButtonGroup bp;
    private boolean rb;
    private static Professor prof;
    private Gamification newGame = new Gamification(prof.getNome(), prof.getCpf());
    private ArrayList<Double> nota = new ArrayList<Double>();

    private FrameInterface(String title) {
        TextButton trata = new TextButton();
        // setando o nome da janela e a visualização
        myFrame = new JFrame(title);
        myFrame.getContentPane().setLayout(new FlowLayout(FlowLayout.CENTER, 10, 5));

        // setando os botões que aparecerão na janela
        save = new JButton("Salvar");
        cancel = new JButton("Cancelar");
        cadastro = new JButton("Novo Jogador");
        gravar = new JButton("Gravar");
        cod = new JButton("Gerar código ID");
        conq = new JButton("Gerar Conquistas");
        remove = new JButton("Remover um aluno");
        cancel.setToolTipText("Clique aqui para sair");
        cadastro.setToolTipText("Novo cadastro de um aluno jogador");
        gravar.setToolTipText("Gravar notas");

        save.addActionListener(trata);
        cancel.addActionListener(trata);
        cadastro.addActionListener(trata);
        gravar.addActionListener(trata);
        cod.addActionListener(trata);
        conq.addActionListener(trata);
        remove.addActionListener(trata);

        // setando os campos de texto
        nomeAluno = new JTextField(25);
        notas = new JTextField(5);
        matricula = new JTextField(10);
        faltas = new JTextField(3);
        atividades = new JTextField(5);

        // setando os dados se é participativo ou não
        yes = new JRadioButton("Sim");
        no = new JRadioButton("Não");
        bp = new ButtonGroup();
        bp.add(yes);
        bp.add(no);
        yes.addActionListener(trata);
        no.addActionListener(trata);

        // adicionando todos os elementos na janela
        JPanel myPanel = new JPanel(new BorderLayout());

        // campo de colocar o nome
        JPanel textName = new JPanel();
        textName.add(new JLabel("Nome do aluno: "));
        textName.add(nomeAluno);

        // campo de colocar a matricula
        JPanel textMat = new JPanel();
        textMat.add(new JLabel("Matrícula do aluno: "));
        textMat.add(matricula);

        // campo de colocar as atividades
        JPanel activities = new JPanel();
        activities.add(new JLabel("Atividades realizadas"));
        activities.add(atividades);

        // campo de faltas
        JPanel textFaltas = new JPanel();
        textFaltas.add(new JLabel("Faltas: "));
        textFaltas.add(faltas);

        // campo das notas
        JPanel textNotas = new JPanel();
        textNotas.add(new JLabel("Notas: "));
        textNotas.add(notas);
        textNotas.add(gravar);

        // campo do radio button
        JPanel participative = new JPanel();
        participative.add(new JLabel("Participativo?"));
        participative.add(yes);
        participative.add(no);

        // botões salvar, cancelar e novo cadastro
        JPanel saveCancelCad = new JPanel();
        saveCancelCad.add(save);
        saveCancelCad.add(cancel);
        saveCancelCad.add(cadastro);

        JPanel p2 = new JPanel();
        p2.add(cod);
        p2.add(conq);
        p2.add(remove);

        myPanel.add(activities, BorderLayout.PAGE_START);
        myPanel.add(textFaltas, BorderLayout.BEFORE_LINE_BEGINS);
        myPanel.add(participative, BorderLayout.AFTER_LINE_ENDS);
        myPanel.add(saveCancelCad, BorderLayout.PAGE_END);

        myFrame.getContentPane().add(textName);
        myFrame.getContentPane().add(textMat);
        myFrame.getContentPane().add(textNotas);
        myFrame.getContentPane().add(myPanel);
        myFrame.getContentPane().add(p2);

        // setando configurações da janela
        myFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        myFrame.setSize(490, 260);
        myFrame.setResizable(false);
        myFrame.setVisible(true);
    }

    private class TextButton implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e){
            // capturando os campos preenchidos
            if (e.getSource() == save){
                try(FileOutputStream out = new FileOutputStream("objetoAlunos.txt", true);
                    ObjectOutputStream obj = new ObjectOutputStream(out))
                {

                    String nome = nomeAluno.getText();
                    String idAluno = matricula.getText();
                    int qtdeAtividades = Integer.parseInt(atividades.getText());
                    int numFaltas = Integer.parseInt(faltas.getText());
                    rb = bp.getSelection().getActionCommand().equals("Sim");

                    Aluno al = new Aluno(nome, idAluno);
                    al.setAtividades(qtdeAtividades);
                    al.setFaltas(numFaltas);
                    al.setParticipa(rb);

                    for (Double aNota : nota)
                        al.addNota(aNota);

                    newGame.professor.alunos.add(al);

                    nota.clear();

                    obj.writeObject(al);
                    obj.flush();
                }
                catch (FileNotFoundException fnf){
                    System.out.println("Erro ao encontrar o arquivo!");
                }
                catch (IOException e1) {
                    e1.printStackTrace();
                }
            }

            if(e.getSource() == gravar){
                nota.add(Double.parseDouble(notas.getText()));
            }

            if (e.getSource() == cadastro){
                Aluno aluno = new Aluno(nomeAluno.getText(), matricula.getText());
                PlayerInterface pi = new PlayerInterface("Bem vindo, " + nomeAluno.getText() + "!", prof, aluno);
            }

            if(e.getSource() == cod){
                JOptionPane.showMessageDialog(null, "Novo código: " + prof.gerarCodigoID());
            }

            if(e.getSource() == cancel){
                myFrame.getDefaultCloseOperation();
            }

            if(e.getSource() == conq){
                newGame.atribuiConquistas();
            }

            if(e.getSource() == remove){
                int cod = Integer.parseInt(JOptionPane.showInputDialog("Informe o código do aluno a ser removido: "));
                prof.alunos.remove(cod-1);
            }
        }
    }

    public static void main(String[] args){
        String profName = JOptionPane.showInputDialog("Bem vindo! Informe o nome e CPF do professor!\nNome:");
        String profCpf = JOptionPane.showInputDialog("CPF:");

        if (profName.length() == 0 && profCpf.length() == 0)
            throw new ProfName("Nome e CPF inválido!\n Por favor, retorne e informe um nome e um CPF válido.");

        else {
            prof = Professor.getInstance(profName, profCpf);
            FrameInterface interfaceTest = new FrameInterface("Gamification - " + prof.getNome());
        }
    }
}
