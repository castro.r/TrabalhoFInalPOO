/*
 * Copyright (c) 2018.
 *
 * Created by Rafael de Castro.
 */

import java.io.Serializable;
import java.util.Observable;

public class Gamification extends Observable implements Serializable {
    private int xpAluno;
    private int levelAluno;

    Professor professor;

    Gamification(String professorNome, String cpf){
        this.levelAluno = 0;
        this.xpAluno = 0;
        professor = Professor.getInstance(professorNome, cpf);
    }

    private void aumentaLevel(int i) {
        this.levelAluno = professor.jogadores.get(i).getLevel();
        this.xpAluno = (xpAluno % 50);
        this.levelAluno += (xpAluno / 50);

        professor.jogadores.get(i).setLevel(levelAluno);
        professor.jogadores.get(i).setXp(xpAluno);
        setChanged();
        this.notifyObservers(" subiu de level!");
    }

    public void atualizaXp(int i){
        this.xpAluno = professor.jogadores.get(i).getXp();
        int qteAtv = professor.alunos.get(i).getAtividades();
        xpAluno += qteAtv * 10;

        if (xpAluno >= 50)
            this.aumentaLevel(i);

        professor.jogadores.get(i).setXp(xpAluno);
    }

    /*
     * Political - se participa de todos os debates e discussões em sala de aula.
     * Bronze Sage - se realizou de 1 a 5 atividades.
     * Silver Sage - se realizou de 6 a 10 atividades.
     * Golden Sage - se realizou 11+ atividades.
     * Royal Knight - se obteve entre 75% e 100% de presença.
     * Supreme Royal Knight - se obteve 100% de presença.
     * Respawn - se obteve média abaixo de 6.0.
     * Tyro - se obteve média entre 6.0 - 7.9.
     * Thunder - se obteve média entre 8.0 - 9.9.
     * Hawking - se obteve média 10.0.
     */
    public void atribuiConquistas(){
        for (int i = 0; i < professor.jogadores.size(); i++){
            // se o aluno for Political, ele ganha +10% de xp
            if(professor.alunos.get(i).isParticipa()) {
                professor.jogadores.get(i).addConquista("Political");
                this.atualizaXp(i);

                int temp = professor.jogadores.get(i).getXp();
                temp += temp*0.1;
                professor.jogadores.get(i).setXp(temp);

                if(professor.jogadores.get(i).getXp() >= 50)
                    this.aumentaLevel(i);
            }

            // se o aluno for Bronze Sage, ele ganha +2% de xp
            if(professor.alunos.get(i).getAtividades() >= 1 && professor.alunos.get(i).getAtividades() <= 5){
                professor.jogadores.get(i).addConquista("Bronze Sage");
                this.atualizaXp(i);

                int temp = professor.jogadores.get(i).getXp();
                temp += temp*0.02;
                professor.jogadores.get(i).setXp(temp);

                if(professor.jogadores.get(i).getXp() >= 50)
                    this.aumentaLevel(i);
            }

            // se o aluno for Silver Sage, ele ganha +5% de xp
            if(professor.alunos.get(i).getAtividades() >= 6 && professor.alunos.get(i).getAtividades() <= 10){
                professor.jogadores.get(i).addConquista("Silver Sage");
                this.atualizaXp(i);

                int temp = professor.jogadores.get(i).getXp();
                temp += temp*0.05;
                professor.jogadores.get(i).setXp(temp);

                if(professor.jogadores.get(i).getXp() >= 50)
                    this.aumentaLevel(i);
            }

            // se o aluno for Golden Sage, ele ganha +9% de xp
            if(professor.alunos.get(i).getAtividades() >= 11){
                professor.jogadores.get(i).addConquista("Golden Sage");
                this.atualizaXp(i);

                int temp = professor.jogadores.get(i).getXp();
                temp += temp*0.09;
                professor.jogadores.get(i).setXp(temp);

                if(professor.jogadores.get(i).getXp() >= 50)
                    this.aumentaLevel(i);
            }

            // se o aluno tem uma presença entre 75% e 100% (considerando ano letivo de 200 dias), ele ganha +13% de xp
            if(professor.alunos.get(i).getFaltas() >= 2 && professor.alunos.get(i).getFaltas() <= 50){
                professor.jogadores.get(i).addConquista("Royal Knight");

                int temp = professor.jogadores.get(i).getXp();
                temp += temp*0.13;
                professor.jogadores.get(i).setXp(temp);

                if(professor.jogadores.get(i).getXp() >= 50)
                    this.aumentaLevel(i);
            }

            // se o aluno tem 100% de presença, ele ganha +30% de xp
            if(professor.alunos.get(i).getFaltas() == 0){
                professor.jogadores.get(i).addConquista("Supreme Royal Knight");

                int temp = professor.jogadores.get(i).getXp();
                temp += temp*0.3;
                professor.jogadores.get(i).setXp(temp);

                if(professor.jogadores.get(i).getXp() >= 50)
                    this.aumentaLevel(i);
            }

            // se obter média abaixo de 6.0, o aluno perde -1% de xp
            if(professor.alunos.get(i).calculaMedia() < 6.0) {
                professor.jogadores.get(i).addConquista("Respawn");
                this.atualizaXp(i);

                int temp = professor.jogadores.get(i).getXp();
                temp -= temp * 0.01;
                professor.jogadores.get(i).setXp(temp);
            }

            // se obter média entre 6 e 7.9, +5% de xp
            if(professor.alunos.get(i).calculaMedia() >= 6.0 && professor.alunos.get(i).calculaMedia() < 8.0){
                professor.jogadores.get(i).addConquista("Tyro");
                this.atualizaXp(i);

                int temp = professor.jogadores.get(i).getXp();
                temp += temp*0.05;
                professor.jogadores.get(i).setXp(temp);

                if(professor.jogadores.get(i).getXp() >= 50)
                    this.aumentaLevel(i);
            }

            // se obter média entre 8 e 9.9, +10% de xp
            if(professor.alunos.get(i).calculaMedia() >= 8.0 && professor.alunos.get(i).calculaMedia() < 10.0){
                professor.jogadores.get(i).addConquista("Thunder");
                this.atualizaXp(i);

                int temp = professor.jogadores.get(i).getXp();
                temp += temp*0.10;
                professor.jogadores.get(i).setXp(temp);

                if(professor.jogadores.get(i).getXp() >= 50)
                    this.aumentaLevel(i);
            }

            // se obter média igual a 10, +15% de xp
            if(professor.alunos.get(i).calculaMedia() == 10.0){
                professor.jogadores.get(i).addConquista("Hawking");
                this.atualizaXp(i);

                int temp = professor.jogadores.get(i).getXp();
                temp += temp*0.15;
                professor.jogadores.get(i).setXp(temp);

                if(professor.jogadores.get(i).getXp() >= 50)
                    this.aumentaLevel(i);
            }
        }
    }

    // lista os jogadores que estão na turma do professor
    public void listarAlunosJogadores() {
        System.out.println("Código\tNome de usuário\t\tClasse\t\tLevel\tConquistas");

        for (int i = 0; i < professor.jogadores.size(); i++)
            System.out.println(professor.jogadores.get(i)); // retorna o código, o nome de usuário, o level e as conquistas
    }

    // remove um usuario do "game"
    public void deletarAluno(int idCod){
        professor.jogadores.remove(idCod-1);
    }
}
