/*
 * Copyright (c) 2018.
 *
 * Created by Rafael de Castro.
 */

import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.*;

public class GamificationTest {

    @Test
    public void atualizaXp() {
        Gamification game = new Gamification("Rafael", "111111111");
        Professor prof = Professor.getInstance("Rafael", "111111111");

        AlunoJogador al1 = new AlunoJogador("Player1", "1111111", "p1", "senha1", 1);
        Aluno a1 = new Aluno("Player1", "1111111");

        a1.setAtividades(4);

        game.professor.alunos.add(a1);
        game.professor.jogadores.add(al1);

        game.atualizaXp(0);

        assertEquals(40, al1.getXp());
        assertEquals(0, al1.getLevel());

        System.out.println("Teste 1 - Ok!");
    }

    @Test
    public void atribuiConquistas() {
        Gamification game = new Gamification("Rafael", "111111111");
        Professor prof = Professor.getInstance("Rafael", "111111111");

        AlunoJogador al1 = new AlunoJogador("Player1", "1111111", "p1", "senha1", 1);
        Aluno a1 = new Aluno("Player1", "1111111");

        a1.setAtividades(1);
        a1.setFaltas(4);
        a1.setParticipa(true);
        a1.addNota(7);
        a1.addNota(9);
        a1.addNota(5);

        game.professor.jogadores.add(al1);
        game.professor.alunos.add(a1);
        game.atribuiConquistas();

        ArrayList<String> test = new ArrayList<String>();
        test.add("Political");
        test.add("Bronze Sage");
        test.add("Royal Knight");
        test.add("Tyro");

        assertEquals(test, al1.getConquistas());
        System.out.println("Teste 2 - Ok!");
    }
}