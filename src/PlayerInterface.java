/*
 * Copyright (c) 2018.
 *
 * Created by Rafael de Castro.
 */

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;

public class PlayerInterface extends JFrame {
    private JTextField nick;
    private JTextField id;
    private JPasswordField pass;
    private JComboBox<String> classes;
    private JButton modUser;
    private JButton modPass;
    private JButton ranking;
    private JButton individualPoints;
    private JButton salvar;

    PlayerInterface(String title, Professor prof, Aluno al){
        ButtonT trata = new ButtonT();
        // setando o nome da janela e a visualização
        setTitle(title);
        getContentPane().setLayout(new FlowLayout(FlowLayout.CENTER, 10, 10));

        // setando a classe do personagem fictício do aluno
        String[] classe = {"--", "Guerreiro", "Mago", "Ninja", "Orc", "Arqueiro"};
        classes = new JComboBox<>(classe);
        JScrollPane lista = new JScrollPane(classes);

        // campos de texto
        pass = new JPasswordField(15);
        pass.setEchoChar('*');
        nick = new JTextField(10);
        id = new JTextField(3);

        // botões
        salvar = new JButton("Salvar");
        modUser = new JButton("Modificar usuário");
        modPass = new JButton("Modificar senha");
        ranking = new JButton("Ranking na turma");
        individualPoints = new JButton("Level atual");
        modUser.addActionListener(trata);
        modPass.addActionListener(trata);
        ranking.addActionListener(trata);
        individualPoints.addActionListener(trata);

        JPanel myPanel = new JPanel(new BorderLayout());

        JPanel textNick = new JPanel();
        textNick.add(new JLabel("Nickname: "));
        textNick.add(nick);

        JPanel textPass = new JPanel();
        textPass.add(new JLabel("Senha: "));
        textPass.add(pass);

        JPanel textCod = new JPanel();
        textCod.add(new JLabel("Código: "));
        textCod.add(id);

        JPanel comboClass = new JPanel();
        comboClass.add(new JLabel("Classe do personagem"));
        comboClass.add(lista);

        JPanel userPass = new JPanel();
        userPass.add(modUser);
        userPass.add(modPass);

        JPanel savePanel = new JPanel();
        savePanel.add(salvar);

        JPanel rankingIndiv = new JPanel();
        rankingIndiv.add(ranking);
        rankingIndiv.add(individualPoints);

        myPanel.add(textNick, BorderLayout.PAGE_START);
        myPanel.add(textPass, BorderLayout.WEST);
        myPanel.add(comboClass, BorderLayout.EAST);
        myPanel.add(savePanel, BorderLayout.SOUTH);

        getContentPane().add(myPanel);
        getContentPane().add(userPass);
        getContentPane().add(rankingIndiv);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setSize(550, 210);
        setResizable(false);
        setVisible(true);
    }

    private class ButtonT implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            if(e.getSource() == salvar){
                try{
                    FileOutputStream out = new FileOutputStream("objetoJogador.txt");
                    ObjectOutputStream obj = new ObjectOutputStream(out);

                    FileInputStream in = new FileInputStream("objetoAlunos.txt");
                    ObjectInputStream ob = new ObjectInputStream(in);

                    Aluno al = (Aluno)ob.readObject();

                    String n = nick.getText();
                    String s = String.valueOf(pass.getPassword());
                    String classe = (String)classes.getSelectedItem();
                    int code = Integer.parseInt(id.getText());

                    AlunoJogador aj = new AlunoJogador(al.getNome(), al.getMatricula(), n, s, code);
                    aj.setClasse(classe);

                    obj.writeObject(aj);

                    out.close();
                    obj.close();
                    in.close();
                    ob.close();
                }
                catch (IOException | ClassNotFoundException e1) {
                    e1.printStackTrace();
                }
            }
            if(e.getSource() == individualPoints){
                try{
                    FileInputStream in = new FileInputStream("objetoJogador.txt");
                    ObjectInputStream obj = new ObjectInputStream(in);

                    AlunoJogador alj = (AlunoJogador)obj.readObject();
                    JOptionPane.showMessageDialog(null, "Level atual do jogador " + alj.getNomeUsuario() + ": " + alj.getLevel());

                    obj.close();
                    in.close();
                }
                catch (FileNotFoundException e1) {
                    e1.printStackTrace();
                }
                catch (IOException e1) {
                    e1.printStackTrace();
                }
                catch (ClassNotFoundException e1) {
                    e1.printStackTrace();
                }
            }
            if(e.getSource() == modUser){
                String newUser = JOptionPane.showInputDialog("Entre com o novo nome de usuário: ");

                try{
                    FileOutputStream out = new FileOutputStream("objetoJogador.txt");
                    ObjectOutputStream obj = new ObjectOutputStream(out);

                    FileInputStream input = new FileInputStream("objetoJogador.txt");
                    ObjectInputStream ob = new ObjectInputStream(input);

                    AlunoJogador alj = (AlunoJogador)ob.readObject();
                    alj.modificarUsuario(newUser);

                    obj.writeObject(alj);

                    ob.close();
                    input.close();
                    obj.close();
                    out.close();
                }
                catch (FileNotFoundException e1) {
                    e1.printStackTrace();
                }
                catch (IOException e1) {
                    e1.printStackTrace();
                }
                catch (ClassNotFoundException e1) {
                    e1.printStackTrace();
                }

            }
            if(e.getSource() == modPass){
                String newPass = JOptionPane.showInputDialog("Entre com a nova senha: ");

                try{
                    FileOutputStream out = new FileOutputStream("objetoJogador.txt");
                    ObjectOutputStream obj = new ObjectOutputStream(out);

                    FileInputStream input = new FileInputStream("objetoJogador.txt");
                    ObjectInputStream ob = new ObjectInputStream(input);

                    AlunoJogador alj = (AlunoJogador)ob.readObject();
                    alj.modificarSenha(newPass);

                    obj.writeObject(alj);

                    ob.close();
                    input.close();
                    obj.close();
                    out.close();
                }
                catch (FileNotFoundException e1) {
                    e1.printStackTrace();
                }
                catch (IOException e1) {
                    e1.printStackTrace();
                }
                catch (ClassNotFoundException e1) {
                    e1.printStackTrace();
                }
            }
        }
    }
}
