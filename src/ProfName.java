/*
 * Copyright (c) 2018.
 *
 * Created by Rafael de Castro.
 */

public class ProfName extends RuntimeException {
    public ProfName(String message){
        super(message);
    }
}
