/*
 * Copyright (c) 2018.
 *
 * Created by Rafael de Castro.
 */

import java.io.Serializable;
import java.util.ArrayList;

public final class Professor implements Serializable {
    private int codigo;
    private String nome;
    private String cpf;
    private static Professor instance;

    ArrayList<Aluno> alunos = new ArrayList<Aluno>();
    ArrayList<AlunoJogador> jogadores = new ArrayList<AlunoJogador>();

    private Professor(String nome, String cpf) {
        this.nome = nome;
        this.cpf = cpf;
        this.codigo = 0;
    }

    // getters
    public String getNome() {
        return nome;
    }

    public String getCpf() {
        return cpf;
    }

    /*
     * gera uma serie de códigos (iniciando em 1)
     * a ideia de utilizar esta sequência é para facilitar a mudança ou busca de algum usuário
     * o codigo é o index do aluno no ArrayList
     */
    public int gerarCodigoID() {
        return ++codigo;
    }

    // reinicia a sequência de códigos (para uma nova turma de alunos)
    public void zerarCodigos() {
        this.codigo = 0;
    }

    // retorna uma instancia única do Professor
    public static Professor getInstance(String name, String cpf) {
        if (instance == null)
            instance = new Professor(name, cpf);

        return instance;
    }

    @Override
    public String toString() {
        return "Nome do professor: " + nome +
                "\nCPF: " + cpf;
    }
}
